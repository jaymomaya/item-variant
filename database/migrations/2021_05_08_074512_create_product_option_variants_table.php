<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductOptionVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_option_variants', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_variant_id');
            $table->foreign('product_variant_id')->on('product_variants')->references('id');
            $table->foreignId('option_id');
            $table->foreign('option_id')->on('options')->references('id');
            $table->string('value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_option_variants');
    }
}
