<?php

namespace Database\Factories\Product;

use App\Models\Product\ProductOptionVariant;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductOptionVariantFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductOptionVariant::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
