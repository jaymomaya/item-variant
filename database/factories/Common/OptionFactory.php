<?php

namespace Database\Factories\Common;

use App\Models\Common\Option;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class OptionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Option::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'is_active' => true,
            'created_by' => User::first()->id,
        ];
    }
}
