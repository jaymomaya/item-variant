@php
    $route = request()->route()->getName();
@endphp
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Item Variant</a>
        </div>
        @auth
            <ul class="nav navbar-nav">
                <li class="{{in_array($route, config('constant_header.home')) ? 'active' : ''}}">
                    <a href="{{route('home')}}">Home</a>
                </li>
                <li class="{{in_array($route, config('constant_header.product')) ? 'active' : ''}}">
                    <a href="{{route('product.index')}}">Product</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{route('logout')}}"><span class="glyphicon glyphicon-log-in"></span> Logout</a>
                </li>
            </ul>
        @else
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{route('login')}}"><span class="glyphicon glyphicon-log-in"></span> Login</a>
                </li>
            </ul>
        @endauth
    </div>
</nav>
