@extends('layouts.auth-app')
@section('title', 'Login')
@section('content')
    <div class="row">
        <div class="login-form col-md-4 col-md-offset-4">
            <form class="form" id="login_form" action="{{ route('login') }}" method="POST">
                @csrf
                <h2>Login to System</h2>
                <div class="row">
                    <div class="col-md-12 form-group">
                        <input class="form-control" type="text" placeholder="Email" id="username" name="email" autocomplete="off" />
                        @if ($errors->has('email'))
                        <span class="text-danger">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-group">
                        <input class="form-control" type="password" id="password" placeholder="Password" name="password" />
                        @if ($errors->has('password'))
                        <span class="text-danger">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <button id="kt_login_signin_submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">Sign In</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
