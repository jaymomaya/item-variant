<?php
return [
    'home' => [
        '/',
        'home',
        'dashboard'
    ],
    'product' => [
        'product.index',
        'product.edit',
        'product.create',
        'product.show',
    ],
];
